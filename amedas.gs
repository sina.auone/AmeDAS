function getAmedas() {
  // アメダスデータのJSON取得　一時間おき
  // http://mindtech.jp/?p=1754
  
  //エリア番号を http://www.jma.go.jp/bosai/common/const/area.json から設定。
  //神戸のエリア番号は63518 
  let area = 63518 ;
  
  //Google Sheetのシート名を設定。
  let book = SpreadsheetApp.getActiveSpreadsheet();
  let sheet1Data = book.getSheetByName("シート1");
  let sheet2Data = book.getSheetByName("シート2");  //最新値用
  
  //----　設定はここまで
  
  //Google Sheet1の2行目に行を挿入する
  sheet1Data.insertRows(2,1);

  //現在の日時を取得し2行目1列目のセルに日時を入力する
  let now = new Date();
  let val_time = Utilities.formatDate(now, 'Asia/Tokyo', 'yyyy/MM/dd HH:mm:ss');
  sheet1Data.getRange(2,1).setValue(val_time);
  sheet2Data.getRange(2,1).setValue(val_time);  //最新値用

  // アメダスのデータは間に合わない時があるので、１時間前のデータを取ってくるようにする。
  // データの日時として2行目2列目のセルに入力する。
  let an_hour_ago = new Date();
  an_hour_ago.setHours(now.getHours() -1);
  
  let val_time2 = Utilities.formatDate(an_hour_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet1Data.getRange(2,2).setValue(val_time2);
  sheet2Data.getRange(2,2).setValue(val_time2);//最新値用

  //リクエストを送る時に付与するパラメータ
  let params = {
    "method" : "get"
  };

  //APIのリクエストURL
  let requestUrl = 'https://www.jma.go.jp/bosai/amedas/data/map/'+ val_time2 +'0000.json';

  //取得したJSONデータを変数に格納
  let response = UrlFetchApp.fetch(requestUrl, params);
  let json = JSON.parse(response.getContentText());

  // 2行3列目にエリア番号を入力
  sheet1Data.getRange(2,3).setValue(area);
  sheet2Data.getRange(2,3).setValue(area); //最新値用

  // 2行4列目に気温を入力
  sheet1Data.getRange(2,4).setValue( json[area]['temp'][0] );
  sheet2Data.getRange(2,4).setValue( json[area]['temp'][0] );//最新値用

  // 2行5列目に湿度を入力
  sheet1Data.getRange(2,5).setValue( json[area]['humidity'][0] );
  sheet2Data.getRange(2,5).setValue( json[area]['humidity'][0] );//最新値用

  // 2行6列目に海面気圧を入力
  sheet1Data.getRange(2,6).setValue( json[area]['normalPressure'][0] );
  sheet2Data.getRange(2,6).setValue( json[area]['normalPressure'][0] );//最新値用

  // 2行7列目に風速を入力
  sheet1Data.getRange(2,7).setValue( json[area]['wind'][0] );
  sheet2Data.getRange(2,7).setValue( json[area]['wind'][0] );//最新値用

}
